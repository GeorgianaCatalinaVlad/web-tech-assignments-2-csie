
const FIRST_NAME = "Vlad";
const LAST_NAME = "Georgiana-Catalina";
const GRUPA = "1085";

/**
 * Make the implementation here
 */
function initCaching() {

    var person={};//empty object
    
    var v={
       
        pageAccessCounter:function(parametru="home"){
                      
            parametru=parametru.toLowerCase();//should not be the case sensitive!!!"HOME"--home(lower)
            //should increase the counter for home page
            //home page-->param=home

            if(!person[parametru]){

                person[parametru]=1;
            }

            else{
            person[parametru]++;
            }
            
        },
        getCache:function()
        {
            return  person;
        }
          
    };
   return v;
}



module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

